'use strict';

require('dotenv').config();

const 
CoffeeScript = require('coffee-script/register'),
bodyParser = require('body-parser'),
crypto = require('crypto'),
express = require('express'),
https = require('https'),
request = require('request'),
cheerio = require('cheerio'),
api_akinator = require('akinator-api'),
api_genius = require('genius-api'),
api_searchYoutube = require('youtube-api-v3-search'),
gapi = require('googleapis'),
fs = require('fs'),
cors = require('cors'),
ytdl = require('ytdl-core'),
path = require('path');

var genius = new api_genius(process.env.GENIUS_ACCESS_TOKEN);

var app = express();
app.set('port', process.env.PORT || 5000);
app.use(bodyParser.json());
app.use(express.static('public'));

/*
 * Be sure to setup your config values before running this code. You can 
 * set them using environment variables or modifying the config file in /config.
 *
 */

// App Secret can be retrieved from the App Dashboard
const APP_SECRET =  process.env.APP_SECRET;

// Arbitrary value used to validate a webhook
const VALIDATION_TOKEN = process.env.VALIDATION_TOKEN;

// Generate a page access token for your page from the App Dashboard
const PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN;

// URL where the app is running (include protocol). Used to point to scripts and 
// assets located at this address. 
const SERVER_URL = process.env.SERVER_URL;

// youtube api key
const YOUTUBE_API_KEY = process.env.YOUTUBE_API_KEY;  

// exceptionnal variable
var last_button_click = null;


// ROUTES
app.get('/',function(req,res){
	res.send("Hi, I'm a chatbot!!!");
});

//FACEBOOK
app.get('/webhook', function(req, res) {
  console.log('hasinanianiaina');
	if (req.query['hub.mode'] === 'subscribe' &&
		req.query['hub.verify_token'] === VALIDATION_TOKEN) {
		console.log("Validating webhook");
	res.status(200).send(req.query['hub.challenge']);
} else {
	console.error("Failed validation. Make sure the validation tokens match.");
	res.sendStatus(403);          
}  
});



/*
 * All callbacks for Messenger are POST-ed. They will be sent to the same
 * webhook. Be sure to subscribe your app to your page to receive callbacks
 * for your page. 
 * https://developers.facebook.com/docs/messenger-platform/product-overview/setup#subscribe_app
 *
 */
 app.post('/webhook', function (req, res) {

 	var data = req.body;
  // Make sure this is a page subscription
  if (data.object == 'page') {
    // Iterate over each entry
    // There may be multiple if batched
    data.entry.forEach(function(pageEntry) {
    	var pageID = pageEntry.id;
    	var timeOfEvent = pageEntry.time;

      // Iterate over each messaging event
      pageEntry.messaging.forEach(function(messagingEvent) {
      	if (messagingEvent.message) {
          if(!messagingEvent.message.quick_reply){
            if(last_button_click == 'parole'){
              var song = messagingEvent.message.text;
              displaySongLists(messagingEvent,song);
            }
          }else{
            if (messagingEvent.message.quick_reply.payload == "back home") {
              displayParts(messagingEvent.sender.id);
            }
          }
      	} else {
          if(messagingEvent.postback ){
            if(messagingEvent.postback.payload === VALIDATION_TOKEN ){
              startedButtonClick(messagingEvent);     
            }else if(messagingEvent.postback.payload === 'akinator'){
              console.log(api_akinator.start());
            }else if(messagingEvent.postback.payload === 'parole'){
              var message = "Tapez le titre de la chanson ou l'artiste";
              sendTextMessage(messagingEvent.sender.id,message);
              last_button_click = 'parole';
            }else if(messagingEvent.postback.payload === 'video_amusante'){
              setOptionsVideo(messagingEvent);
            }else if(messagingEvent.postback.payload.includes('parole selected')){
              displayLyrics(messagingEvent);
            }else if(messagingEvent.postback.payload.includes('video selected')){
              downloadVideo(messagingEvent);
            }
          }
      	}
      });
  });

    // Assume all went well.
    //
    // You must send back a 200, within 20 seconds, to let us know you've 
    // successfully received the callback. Otherwise, the request will time out.
    res.sendStatus(200);
}
});


// ================== Started button clicked ==================
function  startedButtonClick(event){
  var senderID = event.sender.id;
  var recipientID = event.recipient.id;
  request({
    uri: 'https://graph.facebook.com/v2.6/' + senderID,
    qs: { access_token:  PAGE_ACCESS_TOKEN},
    method: 'GET',
    json: true
  }, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var user_firstName = body.first_name;

      //message displayed after clicking started button
      var msg_1 = "hi &#x1F609;, je suis LaBot, un petit robot conçu pour les besoins quotidiens et vous orienté sur Facebook!\#E02";
      
      var msg_2 = "Je suis en phase d'amélioration en ce moment "+ user_firstName +"..., mais tu peux commencer à utiler mes autres fonctionnalitées!!\#E01";
      
      var msg_tab = [];

      msg_tab.push(msg_1,msg_2);

      msg_tab.forEach(function(message){
        sendTextMessage(senderID, message);
      });

      // all services offer by Arona
      displayParts(senderID);

    } else {
      console.error("Failed calling Send API", response.statusCode, response.statusMessage, body.error);
    }
  });
}

// ================ display services offer by Arona ============================
function displayParts(recipientId){
  var messageData = {
     recipient:{
        id:recipientId
    },
    message:{
        attachment:{
            type:"template",
            payload:{
                template_type:"generic",
                elements:[
                    {
                        title:"Arona +",
                        image_url:"https://picsum.photos/200/300",
                        subtitle:"lorem ipsum",
                        buttons:[
                            {
                                type:"postback",
                                title:"Akinator",
                                payload:"akinator"
                            },
                            {
                                type:"postback",
                                title:"Paroles",
                                payload:"parole"
                            },{
                                type:"postback",
                                title:"Videos Amusantes",
                                payload:"video_amusante"
                            }]
                    },{
                        title:"Google",
                        image_url:"https://picsum.photos/200/300",
                        subtitle:"lorem ipsum",
                        buttons:[
                            {
                                type:"postback",
                                title:"Youtube",
                                payload:"youtube"
                            },
                            {
                                type:"postback",
                                title:"Recherches",
                                payload:"recherche_google"
                            },{
                                type:"postback",
                                title:"Traductions",
                                payload:"google_traduction"
                            }]
                    }]
                  }
              }
          }
  };  

  callSendAPI(messageData);
}

// ========================= display song search by client =====================
function songOrArtist(recipientId,elements){
  var messageData = {
     recipient:{
        id:recipientId
    },
    message:{
      attachment:{
          type:"template",
          payload:{
              template_type:"generic",
              elements:elements
            }
          }
      }
  } 

  callSendAPI(messageData);
}


// ================= display Song lists ===============================
function displaySongLists(event,song){
  var elements = [];
  genius.search(song).then(function(response) {
    var hits = response.hits;
    for(var song of hits){
      var result = song["result"];
      var artist_name = result.primary_artist.name;
      var song_image_url = result.header_image_thumbnail_url;
      var song_title = result.full_title;
      var song_id = result.id;
      var recipient_id = event.sender.id;
      var song = 'perfect';

      var element = {
        title:artist_name,
        image_url:song_image_url,
        subtitle:"lorem ipsum",
        buttons:[
        {
          type:"postback",
          title:song_title,
          payload:"parole selected "+song_id
        }]
      };

      elements.push(element);
    };

    songOrArtist(recipient_id,elements);

    setTimeout(function(){
      displayButtonBackHome(recipient_id);
    },4000);

  });
}

// =================== display lyrics selected ==================
function displayLyrics(event){
   // get song ID
   var song_id = event.postback.payload.substring(16);

  // search song
  genius.song(song_id).then(function(response) {

    //make scrapping 
    request(response.song.url,(error,response,html)=> {
    if(!error && response.statusCode == 200){
      const $ = cheerio.load(html);
      const lyrics = $('.lyrics').text();

      //display lyrics
      sendTextMessage(event.sender.id,lyrics);

      //display button back home
      setTimeout(function(){
        displayButtonBackHome(event.sender.id);
      },4000);
    }else{
      console.log(error);
    }
    });  
  });
}
// ================== download video ===========================
function downloadVideo(event){
  var videoId = event.postback.payload.substring(15);
  var vUrl = "https://www.youtube.com/watch?v="+videoId;
  ytdl.getInfo(vUrl, (err, info) => {
    if (err) throw err;
    let format = ytdl.chooseFormat(info.formats, { quality: '134' });
    if (format) {
      ytdl(vUrl,format)
        .pipe(fs.createWriteStream('video.flv'));
        videoUpload(event.sender.id)
    }
  });
}
// ================== upload video to facebook ====================
function videoUpload(recipientId){
  var upload = {
    recipient:{
        id:recipientId
    },
    message:{
      attachment:{
        type:"video", 
        payload:{
          url:"https://obscure-reef-79058.herokuapp.com/video.flv"
        }
      }
    }
  }

  request({
    uri: 'https://graph.facebook.com/v2.6/me/messages',
    qs: { access_token:  PAGE_ACCESS_TOKEN},
    method: 'POST',
    json: upload

  }, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var recipientId = body.recipient_id;
      var messageId = body.message_id;

      if (messageId) {
        console.log("Successfully sent message with id %s to recipient %s", 
          messageId, recipientId);
      } else {
        console.log("Successfully called Send API for recipient %s", 
          recipientId);
      }
    } else {
      console.log(body);
      console.error("Failed calling Send API", response.statusCode, response.statusMessage, body.error);
    }
  });  
}

// ================== set Option for the video =======================
  function setOptionsVideo(event,video = 'video Amusantes'){
    const options = {
      q:video,
      part:'snippet',
      type:'video',
      maxResults: 10,
      videoDuration:'short'
    }

    var elements = [];
    api_searchYoutube(YOUTUBE_API_KEY,options).then((result)=>{
      result.items.forEach(function(data){
        result = data.snippet;
        
        var element = {
          title:result.title,
          image_url:result.thumbnails.medium.url,
          subtitle:result.description,
          buttons:[
          {
            type:"postback",
            title:'Regarder',
            payload:"video selected "+data.id.videoId
          }]
        };
        elements.push(element);
    });

    displayVideoFunny(event.sender.id,elements);
    },(error)=>{
      console.log(error);
    });
  }


// ================= display funny video ===========================
function displayVideoFunny(recipientId,elements){
   var messageData = {
     recipient:{
        id:recipientId
    },
    message:{
      attachment:{
          type:"template",
          payload:{
              template_type:"generic",
              elements:elements
            }
          }
      }
  } 

  callSendAPI(messageData);
}

// ================= display button back home ======================
function displayButtonBackHome(recipientID){
  var messageData = {
    recipient:{
      id:recipientID
    },
    message:{
      text: "Cliquer pour revenir au Menu principale",
      quick_replies:[
        {
          content_type:"text",
          title:"Menu principale",
          payload:"back home",
          image_url:"https://images.vexels.com/media/users/3/140528/isolated/preview/669972133494bf3994d706dedf165f90-inicio-icono-redondo-1-by-vexels.png"
        }
      ]
    }
  }
  callSendAPI(messageData);
}



// ============== display message ====================
function receivedMessage(event) {
	var senderID = event.sender.id;
	var recipientID = event.recipient.id;
	var timeOfMessage = event.timestamp;
	var message = event.message;

	console.log("Received message for user %d and page %d at %d with message:", 
		senderID, recipientID, timeOfMessage);
	console.log(JSON.stringify(message));

	var isEcho = message.is_echo;
	var messageId = message.mid;
	var appId = message.app_id;
	var metadata = message.metadata;

  // You may get a text or attachment but not both
  var messageText = message.text;
  var messageAttachments = message.attachments;
  var quickReply = message.quick_reply;


  if (isEcho) {
    // Just logging message echoes to console
    console.log("Received echo for message %s and app %d with metadata %s", 
    	messageId, appId, metadata);
    return;
} else if (quickReply) {
	var quickReplyPayload = quickReply.payload;
	console.log("Quick reply for message %s with payload %s",
		messageId, quickReplyPayload);

	sendTextMessage(senderID, "Quick reply tapped");
	return;
}

if (messageText) {

    // If we receive a text message, check to see if it matches any special
    // keywords and send back the corresponding example. Otherwise, just echo
    // the text we received.

    var greeting = new RegExp(/^(yo|hola|ola|howdy|hi|hey|hello)/i);
    if(messageText.match(greeting)){
    	sendTextMessage(senderID,'Hello there!');
    }
} else if (messageAttachments) {
	sendTextMessage(senderID, "Message with attachment received");
}
}



/*
 * Send a text message using the Send API.
 *
 */
 function sendTextMessage(recipientId, messageText) {
 	var messageData = {
 		recipient: {
 			id: recipientId
 		},
 		message: {
 			text: messageText,
 			metadata: "DEVELOPER_DEFINED_METADATA"
 		}
 	};

 	callSendAPI(messageData);
 }

/*
 * Call the Send API. The message data goes in the body. If successful, we'll 
 * get the message id in a response 
 *
 */
 function callSendAPI(messageData) {
 	request({
 		uri: 'https://graph.facebook.com/v2.6/me/messages',
 		qs: { access_token:  PAGE_ACCESS_TOKEN},
 		method: 'POST',
 		json: messageData

 	}, function (error, response, body) {
 		if (!error && response.statusCode == 200) {
 			var recipientId = body.recipient_id;
 			var messageId = body.message_id;

 			if (messageId) {
 				console.log("Successfully sent message with id %s to recipient %s", 
 					messageId, recipientId);
 			} else {
 				console.log("Successfully called Send API for recipient %s", 
 					recipientId);
 			}
 		} else {
 			console.error("Failed calling Send API", response.statusCode, response.statusMessage, body.error);
 		}
 	});  
 }



app.listen(app.get('port'),function(){
	console.log('running port');
});